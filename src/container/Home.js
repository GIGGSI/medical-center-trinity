import ReviewsSection from '../components/Globals/ReviewsSection/ReviewsSection'
import { AboutUs, BlogSection, Contacts, Services, Slider, Team } from '../components/Home'
import PatientInformation from '../components/Home/PatientInformation/PatientInformation'

const Home = () => {
    return (
        <div id="scrollContainer">
            <Slider id='home' />
            <AboutUs id='about' />
            <PatientInformation />
            <Team id="team" />
            <Services id="services" />
            <ReviewsSection />
            <BlogSection id="blog" />
            <Contacts id="contact" />
        </div>
    )
}

export default Home