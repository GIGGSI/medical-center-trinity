import React from 'react'
import { useParams } from 'react-router-dom'
import { BlogSection, Team } from '../../components/Home'
import Content from '../../components/SingleDctor/Content/Content'

import { ourTeam } from '../../components/Home/Team/TeamConstants'

import './SingleDoctor.css'

const SingleDoctor = () => {
    let { name } = useParams()
    let singleDoctor = ourTeam.filter((item) => item.urlName === name);

    return (
        <div className='doctor-wrapper'>

            <Content data={singleDoctor} />
            <Team />
            <BlogSection />
        </div>
    )
}

export default SingleDoctor