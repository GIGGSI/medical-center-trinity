import React from 'react'
import BlogWrapper from '../../components/Blog/SingleBlog/BlogWrapper'
import { useParams } from 'react-router-dom'
import { blogData } from '../../components/Home/BlogSection/Blogs'

import './SingleBlog.css'

const SingleBlog = () => {
    const { id } = useParams();
    const blog = blogData.filter((item) => item.id === +id)[0]

    return (
        <div className='single-blog-page-wrapper'>
            <BlogWrapper {...blog} />
        </div>
    )
}

export default SingleBlog