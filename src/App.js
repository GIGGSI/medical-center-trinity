import './App.css';
import { Routes, Route } from "react-router-dom";


import { Navbar, Footer } from './components/Globals'
import Home from './container/Home';
import SingleDoctor from './container/SingleDoctor/SingleDoctor';
import ScrollToTop from './utils/ScrollToTop';
import SingleBlog from './container/SingleBlog/SingleBlog';
import Blog from './container/Blog';
import PriceList from './container/PriceList';

function App() {
  return (
    <>
      <ScrollToTop />
      <Navbar />
      <Routes>
        <Route exact path='/' element={<Home />} />
        <Route path='/:name' element={<SingleDoctor />} />
        <Route path='/single-blog/:id' element={<SingleBlog />} />
        <Route path='/blog' element={<Blog />} />
        <Route path='/price-list' element={<PriceList />} />
      </Routes>
      <Footer />
    </>
  );
}

export default App;