import logo from '../assets/logo-trinity.png'
import trainingWoman from '../assets/blog/training-woman.jpg'
import womanDrinkingPillows from '../assets/blog/woman-drinking-pillows.png'
import follat from '../assets/blog/follat.jpeg'
import follatFoods from '../assets/blog/follat-foods.jpeg'
import womanBreastfeeding from '../assets/blog/woman-breastfeeding.jpg'
import womanBreastfeeding2 from '../assets/blog/woman-breastfeeding2.jpg'
import man from '../assets/blog/man.jpg'
import man2 from '../assets/blog/man2.jpg'
import img2 from '../assets/slider/2.jpg'
import imageRigth from '../assets/banner-right.jpeg'
import doc from '../assets/doc.jpeg'



export default {
    logo,
    trainingWoman,
    womanDrinkingPillows,
    follatFoods,
    follat,
    womanBreastfeeding,
    womanBreastfeeding2,
    man,
    man2,
    img2,
    imageRigth,
    doc
}