import React from 'react'
import { blogData } from '../../Home/BlogSection/Blogs'

import './BlogPageContent.css'
import SingleBlogPageContent from './SingleBlogPageContent'

const BlogPageContent = () => {
    return (
        <div className='blog-page-content'>
            {blogData.map((item) => (
                <SingleBlogPageContent key={item.id} {...item} />
            ))}

        </div>
    )
}

export default BlogPageContent