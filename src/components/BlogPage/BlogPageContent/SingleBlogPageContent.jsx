import React from 'react'
import { Link } from 'react-router-dom'

import './SingleBlogPageContent.css'

const SingleBlogPageContent = ({ title, info, imgUrl, id }) => {
    return (
        <div className='single-blog-page-content'>
            <Link
                to={`/single-blog/${id}`}
                className='single-blog-page-image-container'
            >
                <img src={imgUrl} alt={title} />
            </Link>
            <div className='single-blog-page-content-container'>

           
                    <Link className='single-blog-page-content-container-title'
                        to={`/single-blog/${id}`}>
                      {title}
                    </Link>
        

                <p className='single-blog-page-content-container-info'>
                    {info.length > 250
                        ? info.slice(0, 250) + '...'
                        : info
                    }
                </p>

            </div>
        </div>
    )
}

export default SingleBlogPageContent