import BlogPageContent from './BlogPageContent/BlogPageContent'
import RecommendedBlogSection from '../Blog/RecommendedBlogSection/RecommendedBlogSection'

import './BlogPage.css'

const BlogPage = () => {
    return (
        <div className='blog-page-wrapper'>
            <BlogPageContent />

            <RecommendedBlogSection />
        </div>
    )
}

export default BlogPage