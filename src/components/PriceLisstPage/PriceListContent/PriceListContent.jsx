import React from 'react'

import './PriceListContent.css'

const PriceListContent = ({ isOpenContent, content, id, price }) => {
  return (
    <div className='price-list-content-wrapper'>
      {isOpenContent === id && <div>
        <p>  {content}</p>
        <p> {price} лв. </p>
      </div>}
    </div>
  )
}

export default PriceListContent