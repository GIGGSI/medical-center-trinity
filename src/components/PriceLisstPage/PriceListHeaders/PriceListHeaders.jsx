import React from 'react'
import { Link } from 'react-router-dom'

import { HiPlus, HiMinus } from 'react-icons/hi'

import './PriceListHeaders.css'

const PriceListHeaders = ({ title, toggle, id, isOpenContent }) => {
    return (
        <div className='price-list-headers-wrapper'>
            <p
                onClick={() => toggle(id)}
                className='price-list-wrapper-single-title'
            >
                {isOpenContent === id
                    ? <HiMinus className='price-list-headers-icon' />
                    : <HiPlus className='price-list-headers-icon' />}
                {title}
            </p>
        </div>
    )
}

export default PriceListHeaders