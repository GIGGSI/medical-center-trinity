import { useState } from 'react'
import PriceListContent from '../PriceListContent/PriceListContent'
import PriceListHeaders from '../PriceListHeaders/PriceListHeaders'
import { servicesData } from '../../Home/Services/ServicesConstants'

import './PriceListPage.css'

const PriceListPage = () => {
    const [isOpenContent, setIsOpenContent] = useState(false)

    const toggle = index => {
        if (isOpenContent === index) {
            return setIsOpenContent(null);
        }
        setIsOpenContent(index);
    };

    return (
        <div className='price-list-wrapper'>
            {servicesData.map((item) => (
                <div key={item.id} className="price-list-wrapper-single">

                    <PriceListHeaders
                        {...item}
                        toggle={toggle}
                        isOpenContent={isOpenContent}
                    />
                    <PriceListContent
                        {...item}
                        isOpenContent={isOpenContent}

                    />

                </div>
            ))}
        </div>
    )
}

export default PriceListPage