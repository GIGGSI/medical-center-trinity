import React from 'react'
import RecommendedBlogs from '../RecommendedBlogs/RecommendedBlogs'
import RecommendedBlogTitle from '../RecommendedBlogTitle/RecommendedBlogTitle'
import RecommendedSocialMediaSection from '../RecommendedSocialMediaSection/RecommendedSocialMediaSection'

import './RecommendedBlogSection.css'

const RecommendedBlogSection = () => {
    return (
        <div className='recommended-blogs'>
            <RecommendedSocialMediaSection/>
            
            <RecommendedBlogTitle title='Препоръчани статии' />
            <RecommendedBlogs />
        </div>
    )
}

export default RecommendedBlogSection