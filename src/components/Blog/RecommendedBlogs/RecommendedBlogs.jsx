import React from 'react'
import { Link } from 'react-router-dom'
import { blogData } from '../../Home/BlogSection/Blogs'

import './RecommendedBlogs.css'

const RecommendedBlogs = () => {

  const recommendedBlogs = blogData.filter((item) => item.is_recommended)

  return (
    <ul className='recommended-blogs-content'>
      {recommendedBlogs.map((item) => (
        <Link to={`/single-blog/${item.id}`}
          key={item.id}
          className="recommended-blogs-wrapper"
        >
          <div className='recommended-blogs-image-container'>
            <img
              src={item.imgUrl}
              alt={item.title}
            />
          </div>
          <p className='recommended-blogs-title'>
            {item.title}
          </p>
        </Link>
      ))}
    </ul>

  )
}

export default RecommendedBlogs