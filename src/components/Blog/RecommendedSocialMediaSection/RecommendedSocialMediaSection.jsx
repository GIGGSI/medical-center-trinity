
import RecommendedBlogTitle from '../RecommendedBlogTitle/RecommendedBlogTitle'

import { TiSocialFacebook } from 'react-icons/ti'
import { AiOutlineInstagram } from 'react-icons/ai'
import './RecommendedSocialMediaSection.css'

const RecommendedSocialMediaSection = () => {
    return (
        <div className='recommended-social-media-section'>
            <RecommendedBlogTitle title="Социални медии" />

            <div className='recommended-social-media-wrapper'>
                <a
                    href='https://www.facebook.com/medical.center.trinity'
                    className='recommended-social-media-facebook'
                    target="_blank"
                    rel="noreferrer"
                >
                    <TiSocialFacebook className='recommended-social-media-icon' />
                    |   facebook
                </a>
                <a
                    href='https://www.instagram.com/medical.center.trinity/'
                    className='recommended-social-media-instagram'
                    target="_blank"
                    rel="noreferrer"
                >
                    <AiOutlineInstagram className='recommended-social-media-icon' />
                    |   instagram
                </a>
            </div>
        </div>
    )
}

export default RecommendedSocialMediaSection