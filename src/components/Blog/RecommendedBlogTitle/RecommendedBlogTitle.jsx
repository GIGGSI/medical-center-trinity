
import './RecommendedBlogTitle.css'

const RecommendedBlogTitle = ({ title }) => {
    return (
        <div className='recommended-blog-heading'>
            <p>{title}</p> 
        </div>
    )
}

export default RecommendedBlogTitle