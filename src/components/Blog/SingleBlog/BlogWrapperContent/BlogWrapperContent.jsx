import React from 'react'

import './BlogWrapperContent.css'

const BlogWrapperContent = ({ data }) => {
    return (
        <div className='blog-wrapper-content'>
            {data.map((item) => (
                <div key={item.id}>

                    <p className='blog-wrapper-content-heading' >{item?.heading}</p>
                    <div className='blog-wrapper-content-text'
                        dangerouslySetInnerHTML={{
                            __html: item?.text
                        }}></div>
                </div>
            ))}
        </div>
    )
}

export default BlogWrapperContent