import React from 'react'
import BlogImageContainer from './BlogImageContainer/BlogImageContainer'
import BlogWrapperConentContainer from './BlogWrapperConetntContainer/BlogWrapperConentContainer'
import SingleBlogTitle from './SingleBlogTitle/SingleBlogTitle'
import RecommendedBlogSection from '../RecommendedBlogSection/RecommendedBlogSection'

import './BlogWrapper.css'


const BlogWrapper = ({ title, imgUrl, info, content, secondImg }) => {
    return (
        <div className='blog-wrapper'>
            <BlogImageContainer imgPath={imgUrl} title={title} topImage/>

            <SingleBlogTitle title={title} info={info} />

            <div className='blog-wrapper-grid'>
                <BlogWrapperConentContainer
                    content={content}
                    secondImg={secondImg}
                    title={title}
                />

                <RecommendedBlogSection />
            </div>

        </div>
    )
}

export default BlogWrapper