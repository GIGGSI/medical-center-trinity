import React from 'react'
import BlogWrapperContent from '../BlogWrapperContent/BlogWrapperContent'
import BlogImageContainer from '../BlogImageContainer/BlogImageContainer'

import './BlogWrapperContentContainer.css'

const BlogWrapperConentContainer = ({ content, secondImg,title }) => {
    return (
        <div className='blog-wrapper-content-container'>
            <BlogWrapperContent data={content.slice(0, 3)} />
            {
                secondImg
                && <BlogImageContainer title={title} imgPath={secondImg} />
            }

            <BlogWrapperContent data={content.slice(3)} />
        </div>)
}

export default BlogWrapperConentContainer