import React from 'react'

import './SingleBlogTitle.css'
const SingleBlogTitle = ({ title, info }) => {
  return (
    <div className='blog-wrapper-container'>
      <p className='blog-wrapper-container-title'>
        {title}
      </p>
      <p className='blog-wrapper-container-info'>{info}</p>
    </div>
  )
}

export default SingleBlogTitle