import React from 'react'

import '../BlogWrapper.css'
import './BlogImageContainer.css'

const BlogImageContainer = ({ imgPath, title, topImage }) => {
    return (
        <div className={topImage? 'blog-wrapper-image-container top-image': 'blog-wrapper-image-container'}>
            <img
                src={imgPath}
                alt={title}
                className="blog-wrapper-image-container-image"
            />
        </div>
    )
}

export default BlogImageContainer