import { useState, useEffect } from 'react';

import SectionReviewIconContainer from './SectionReviewIconContainer';

import { GrBlockQuote } from 'react-icons/gr'
import { reviewsData } from './ReviewsData';
import './ReviewsSection.css'
import ReviewContainer from './ReviewContainer';

const ReviewsSection = () => {
    const [people, setPeople] = useState(reviewsData);
    const [index, setIndex] = useState(0);

    useEffect(() => {
        const lastIndex = people.length - 1;
        if (index < 0) {
            setIndex(lastIndex)
        }
        if (index > lastIndex) {
            setIndex(0)
        }

    }, [index, people]);

    useEffect(() => {
        let slider = setInterval(() => {
            setIndex(index + 1)
        }, 6000);
        return () => clearInterval(slider)
    }, [index])

    return (
        <section className="section-reviews">

            <div className="section-center-reviews">
                <div className='section-icon-container'>
                    <GrBlockQuote className="icon-reviews" />
                </div>
                {people.map((person, personIndex) => {
                    const { id, image, name, title, quote } = person;

                    let position = 'nextSlide';

                    if (personIndex === index) {
                        position = 'activeSlide'
                    }
                    if (personIndex === index - 1 || (index === 0 &&
                        personIndex === people.length - 1)) {
                        position = 'lastSlide'
                    }

                    return (
                        <ReviewContainer
                            key={id}
                            position={position}
                            image={image}
                            name={name}
                            title={title}
                            quote={quote}
                        />
                    )
                })}
            </div>
            <div className='section-review-dots-container'>
                <div>
                    {people.map((slide, i) => (
                        <SectionReviewIconContainer
                            key={i}
                            i={i}
                            index={index}
                            setIndex={setIndex}
                        />
                    ))}
                </div>
            </div>
        </section>
    )
}

export default ReviewsSection