import React from 'react'
import { AiOutlineLine } from 'react-icons/ai'

import './ReviewsSection.css'

const SectionReviewIconContainer = ({ i, setIndex, index }) => {
    return (
        <AiOutlineLine
            className={i === index ?
                `section-review-line-icon section-review-line-icon-active`
                : `section-review-line-icon`
            }
            onClick={() => setIndex(i)}
        />
    )
}

export default SectionReviewIconContainer