import React from 'react'

const ReviewContainer = ({ position, quote, image, name, title }) => {
    return (
        <article
            className={`article-reviews ${position}`}>
            <p className="text-reviews">{quote}</p>

            <div className='section-container-review-wrapper'>
                <div className='person-img-container'>
                    <img src={image}
                        alt={name}
                        className="person-img" />
                </div>

                <div>
                    <h2 className="name">{name}</h2>
                    <h3 className="title-position">{title}</h3>
                </div>
            </div>
        </article>
    )
}

export default ReviewContainer