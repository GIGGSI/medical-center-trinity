export const reviewsData = [
    {
        id: 1,
        image:
            'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        name: 'Desislava Docheva',
        title: 'office manager',
        quote:
            'Доктор Маркова е едно бижу! Професионалист, който респектира с точността и умението да обясни всичко, така че развълнувана до притеснение бременна да я разбере. Лекар незабравил човещината! Благодаря и от сърце, че бе до мен през цялата ми бременност!!! Да са здрави тя и съпруга и двете им деца!!!',
    },
    {
        id: 2,
        image:
            'https://images.unsplash.com/photo-1493863641943-9b68992a8d07?ixlib=rb-1.2.1&auto=format&fit=crop&w=739&q=80',
        name: 'Dilyana Stoyanova Lalova',
        title: 'photographer',
        quote:
        'Страхотни специалисти с изключителен подход към пациента!!!'
    },
    {
        id: 3,
        image:
            'https://images.unsplash.com/photo-1519058082700-08a0b56da9b4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80',
        name: 'Jakob Owens',
        title: 'product designer',
        quote:
            'Доктор Маркова е едно бижу! Професионалист, който респектира с точността и умението да обясни всичко, така че развълнувана до притеснение бременна да я разбере. Лекар незабравил човещината! Благодаря и от сърце, че бе до мен през цялата ми бременност!!! Да са здрави тя и съпруга и двете им деца!!!',
    },
    {
        id: 4,
        image:
            'https://images.unsplash.com/photo-1469334031218-e382a71b716b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
        name: 'Atikh Bana',
        title: 'traveler',
        quote:
            'Доктор Маркова е едно бижу! Професионалист, който респектира с точността и умението да обясни всичко, така че развълнувана до притеснение бременна да я разбере. Лекар незабравил човещината! Благодаря и от сърце, че бе до мен през цялата ми бременност!!! Да са здрави тя и съпруга и двете им деца!!!',
    },
];

