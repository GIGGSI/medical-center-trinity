import { BsFacebook, BsInstagram } from 'react-icons/bs'

export const followUs = [
    {
        id: 1,
        title: 'Facebook',
        imgPath: <BsFacebook />,
        imageUrl: 'https://www.facebook.com/medical.center.trinity'
    },
    {
        id: 2,
        title: 'Instagram',
        imgPath: <BsInstagram />,
        imageUrl: 'https://www.instagram.com/medical.center.trinity/'
    }
]