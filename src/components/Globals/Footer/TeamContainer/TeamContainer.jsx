import React from 'react'
import { Link } from 'react-router-dom'

import { ourTeam } from '../../../Home/Team/TeamConstants'

import './TeamContainer.css'

const TeamContainer = () => {
    return (
        <div className='team-container'>
            <p className='team-container-title'>
                Нашият Екип
            </p>
            <ul className='team-container-content'>
                {ourTeam.map((item) => (
                    <li key={item.id}>
                        <Link
                            to={`/${item.urlName}`}
                            className='team-container-content-name'
                        >
                            {item.name}
                        </Link>
                    </li>

                ))}

            </ul>
        </div>
    )
}

export default TeamContainer