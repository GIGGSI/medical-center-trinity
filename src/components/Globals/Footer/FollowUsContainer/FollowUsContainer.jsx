
import { followUs } from '../FooterConstants'

import './FollowUsContainer.css'

const FollowUsContainer = () => {
    return (
        <div className='footer-followUs-wrapper'>
            <p className='footer-followUs-wrapper-title'>
                Последвайте ни:
            </p>

            <div className='footer-followUs-wrapper-content'>
                {
                    followUs.map((item) => (
                        <a
                            href={item.imageUrl}
                            key={item.id}
                            target="_blank"
                            rel="noreferrer"
                        >
                            {item.imgPath}
                            {/* <p className='footer-followUs-wrapper-p'>{item.title}</p> */}
                        </a>
                    ))
                }
            </div>
        </div>
    )
}

export default FollowUsContainer