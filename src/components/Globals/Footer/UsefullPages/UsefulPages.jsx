import React from 'react'
import { Link } from 'react-router-dom'

import './UsefulPages.css'

const UsefulPages = () => {
    return (
        <div className='footer-useful-pages'>
            <p className='shortcuts-wrapper-title'>
                Полезни страници
            </p>
            <ul className='team-container-content'>
                <li>
                    <Link
                        to='/blog'
                        className='team-container-content-name'
                    >
                        Блог
                    </Link>
                </li>
                <li>
                    <Link
                        to='/price-list'
                        className='team-container-content-name'
                    >
                        Ценова листа
                    </Link>
                </li>
            </ul>
        </div>
    )
}

export default UsefulPages