import React from 'react'
import { useLocation } from 'react-router-dom'

import { HashLink } from 'react-router-hash-link';

import { navbarLinks, scrollToSection } from '../../Navbar/NavbarConstants'

import './Shortcuts.css'

const Shortcuts = () => {
    let location = useLocation();
    return (
        <div className='shortcuts-wrapper'>
            <p className='shortcuts-wrapper-title'>
                Бързи Връзки
            </p>
            <div className='shortcuts-content'>
                {navbarLinks.map((item) => (
                    <li
                        key={item.id}
                    >
                        <HashLink
                            to={location.pathname !== '/' ? `/${item.footerUrl}` : '/'}
                            onClick={() => location.pathname === '/' && scrollToSection(item.url)}
                            scroll={(el) => el.scrollIntoView({ behavior: 'auto', block: 'end' })}
                            elementId={location.pathname === '/' ? `${item.footerUrl}` : ''}
                        >
                            {item.title}
                        </HashLink>

                    </li>
                ))}
            </div>
        </div>
    )
}

export default Shortcuts