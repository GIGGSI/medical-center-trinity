import FollowUsContainer from './FollowUsContainer/FollowUsContainer'
import TeamContainer from './TeamContainer/TeamContainer'
import Shortcuts from './Shortcuts/Shortcuts'
import UsefulPages from './UsefullPages/UsefulPages'

import './Footer.css'

const Footer = () => {
  return (
    <div className='footer-wrapper'>
      <div className='footer-content'>
        <UsefulPages />
        <Shortcuts />
        <TeamContainer />
        <FollowUsContainer />

      </div>
    </div>
  )
}

export default Footer