import Navbar from "./Navbar/Navbar"
import Footer from "./Footer/Footer"
import Spinner from "./Spinner/Spinner"
import Title from "./Title/Title"

export {
    Navbar,
    Footer,
    Spinner,
    Title,
}