export const navbarLinks = [
    // {
    //     id: 1,
    //     title: 'Home',
    //     url: 'home',
    //     footerUrl:'#home'
    // },
    {
        id: 1,
        title: 'За нас',
        url: 'about',
        footerUrl:'#about'
    },
    {
        id: 2,
        title: 'Екип',
        url: 'team',
        footerUrl:'#team'
    },
    {
        id: 3,
        title: 'Услуги',
        url: 'services',
        footerUrl:'#services'
    },
    {
        id: 4,
        title: 'Блог',
        url: 'blog',
        footerUrl:'#blog'
    },
    {
        id: 5,
        title: 'Контакти',
        url: 'contact',
        footerUrl:'#contact'
    }
]


export const scrollToSection = (id) => {
    let anchorTarget = document.getElementById(id);
    if (anchorTarget) {
        anchorTarget.scrollIntoView({ behavior: "smooth", block: "start" });
        document.getElementById('scrollContainer')
            .scrollTo({
                left: 0,
                top: anchorTarget.offsetTop ,
                behavior: 'smooth'
            });
    }
}
