import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Chevron from './Chevron'
import './Navbar.css';
import Icon from './Icon';
import { navbarLinks, scrollToSection } from './NavbarConstants';
import { HashLink } from 'react-router-hash-link';
import { useLocation } from 'react-router-dom'
import images from '../../../constants/images'

export default function Navbar() {
    const [click, setClick] = useState(false);
    const [button, setButton] = useState(true)
    const [heigth, setHeight] = useState();
    const [showChevron, setShowChevron] = useState(false);
   let container = React.createRef();

    let location = useLocation();

    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);

    const scrollToTop = id => {
        scrollToSection(id)
    }

    const showButton = () => {
        if (window.innerWidth <= 768) {
            setButton(false)
        } else {
            setButton(true)
        }
    };

    useEffect(() => {
        showButton()

        return () => window.removeEventListener('resisze', showButton)

    }, [])

    window.addEventListener('resisze', showButton)

    const showChevrone = () => {
        setHeight(window.screenY)
        if (window.scrollY > 500) {
            setShowChevron(true)
        } else {
            setShowChevron(false)
        }
    }

    useEffect(() => {
        showChevrone()
        return () => window.removeEventListener('scroll', showChevrone)
    }, [heigth])

    window.addEventListener('scroll', showChevrone)

   const handleClickOutside = (event) => {
        if (
            container.current &&
            !container.current.contains(event.target) &&
            !(event.target.className === "menu-icon")
        ) {
            closeMobileMenu()
        }
    };

    useEffect(() => {
        document.addEventListener("mousedown", handleClickOutside);

        return () => document.removeEventListener("mousedown", handleClickOutside)

    })
    return (
        <div className='navbar' 
        >
            <div className="navbar-container container">

                <Link to='/' className="navbar-logo" onClick={closeMobileMenu}>
                    <img src={images.logo} alt="Medical Center Trinity" />
                </Link>
                <div
                    className="menu-icon" ref={container}
                    onClick={handleClick}
                >
                    <Icon open={click} />
                </div>
                <ul className={click ? 'nav-menu active' : 'nav-menu'} >
                    {navbarLinks.map((item) => (
                        <li
                            key={item.id}
                            className="nav-item"
                            onClick={() => {
                                scrollToTop(item.url)
                                closeMobileMenu()
                            }}
                        >
                            <HashLink
                                to={location.pathname !== '/' ? `/${item.footerUrl}` : '/'}
                                onClick={() => location.pathname === '/' && scrollToSection(item.url)}
                                scroll={(el) => el.scrollIntoView({ behavior: 'auto', block: 'end' })}
                                elementId={location.pathname === '/' ? `${item.footerUrl}` : ''}
                                className="nav-links"
                            >
                                {item.title}
                            </HashLink>

                        </li>
                    ))}

                    <li className='save-hours-mobile'>
                        <a
                            href='tel:0895690009'
                        >
                            Запази час

                        </a>
                    </li>
                </ul>
                <div className='save-hours'>
                    <a
                        href='tel:0895690009'
                    >
                        <p><span>Запази час</span></p>

                    </a>
                </div>
            </div>
            {showChevron && <Chevron />}

        </div>
    )
}