import { navbarLinks, scrollToSection } from './NavbarConstants'
import { useLocation } from 'react-router-dom'

import { HashLink } from 'react-router-hash-link';

import './Navbar.css'

const NavbarLinks = ({ className, setToggleMenu, isMobile, toggleMenu }) => {
    let location = useLocation();

    const scrollToTop = id => {
        if (isMobile) {
            setToggleMenu(false)
        }
        scrollToSection(id)
    }
    return (
        <div>
            <ul className={className}>
                {navbarLinks.map((item) => (
                    <li
                        key={item.id}
                        className='p__opensans'
                        onClick={() => scrollToTop(item.url)}
                    >
                        <HashLink
                            to={location.pathname !== '/' ? `/${item.footerUrl}` : '/'}
                            onClick={() => location.pathname === '/' && scrollToSection(item.url)}
                            scroll={(el) => el.scrollIntoView({ behavior: 'auto', block: 'end' })}
                            elementId={location.pathname === '/' ? `${item.footerUrl}` : ''}
                        >
                            {item.title}
                        </HashLink>
                    </li>

                ))}
                {/* {
                    <div>
                        <a
                            href='tel:0895690009'
                        >
                            <p><span className='p__opensans'>Запази час</span></p>

                        </a>
                    </div>
                } */}

            </ul>

        </div>
    )
}

export default NavbarLinks