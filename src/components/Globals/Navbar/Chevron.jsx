import { IoChevronUpCircleSharp } from 'react-icons/io5'
import './Chevron.css'

const Chevron = () => {
    return (
        <div className='chevron-wrapper'>
            <IoChevronUpCircleSharp onClick={() => window.scrollTo(0, 0)} />
        </div>
    )
}

export default Chevron