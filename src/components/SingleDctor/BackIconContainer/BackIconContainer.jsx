import { Link } from 'react-router-dom'

import { AiOutlineArrowLeft } from 'react-icons/ai'
import './BackIconContainer.css'

const BackIconContainer = () => {
    return (
        <div className='back-icon-container tooltip'>
            <Link to='/'>
                <AiOutlineArrowLeft />
            </Link>
            <span className='back-icon-container-info'>
                Обратно към началната страница!
            </span>


        </div>
    )
}

export default BackIconContainer