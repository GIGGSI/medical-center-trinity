import React from 'react'

import BackIconContainer from '../BackIconContainer/BackIconContainer'
import './Content.css'

const Content = ({ data }) => {
    return (
        <div className='content-wrapper'>
         
            <BackIconContainer/>
            <div className='content-wrapper-content-container'>
                <div className='content-image'>
                    <img
                        src={data[0]?.headerImage}
                        alt={data[0]?.name}
                    />
                </div>
                <div className='content-container'>
                    <h1>
                        {data[0].name}
                    </h1>
                    <ul >
                        {data[0]?.content.map((item) => (
                            <li key={item.id}>
                                <p className='content-container-info'>{item.text}</p>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>


        </div>
    )
}

export default Content