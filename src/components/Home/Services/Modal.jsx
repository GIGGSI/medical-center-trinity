import React from 'react'

import './Modal.css'

const Modal = ({ title, content, setShowModal }) => {
    return (
        <div className='Modal' onClick={() => setShowModal(false)}>
            <div className='Component'>
                <span>X</span>
                <p className='modal-component-title'> {title}</p>
                <p className='modal-component-content'>{content}</p>
            </div>

        </div>
    )
}

export default Modal