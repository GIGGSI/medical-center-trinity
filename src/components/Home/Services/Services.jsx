import { useState, useEffect } from 'react'

import { Title } from '../../Globals'
import Modal from './Modal'
import { servicesData } from './ServicesConstants'

import './Services.css'

const Services = ({ id }) => {
    const [identificator, setIdentificator] = useState(1);
    let filteredData = servicesData.filter((item) => item.id === identificator);
    const [isModal, setIsModal] = useState(false)
    const [showModal, setShowModal] = useState(false);
    const handleKeyup = e => e.keyCode === 27 && setShowModal(false);
    const toggleModal = () => setShowModal(!showModal);

    useEffect(() => {
        if (showModal) window.addEventListener('keyup', handleKeyup);
        return () => window.removeEventListener('keyup', handleKeyup);
    });

    const showModalComponent = () => {
        if (window.innerWidth <= 766) {
            setIsModal(true)
        } else {
            setIsModal(false)
        }
    };

    useEffect(() => {
        showModalComponent()

        return () => window.removeEventListener('resisze', showModalComponent)

    }, [])

    window.addEventListener('resisze', showModalComponent)


    return (
        <div id={id} className="services-wrapper">
            <Title title="Нашите услуги" />
            <div className='services-content-wrapper'>
                <div className='services-content'
                >
                    {servicesData.map((item) => (
                        <div
                            key={item.id}
                            className={item.id === identificator
                                ? `services-single-content active`
                                : `services-single-content`}
                            onClick={() => {
                                setIdentificator(item.id)
                                toggleModal()
                            }}
                        >
                            <div
                                className={item.id === identificator
                                    ? `services-single-content-image-container active-image-container`
                                    : `services-single-content-image-container`}
                            >
                                <img src={item.imagePath} alt={item.title} />
                            </div>
                            <p
                                className={item.id === identificator
                                    ? `services-content-title active-title`
                                    : `services-content-title`}
                            >
                                {item.title}
                            </p>

                        </div>
                    ))}

                </div>
                {isModal && showModal ? <Modal title={filteredData[0]?.title}
                    content={filteredData[0]?.content}
                    setShowModal={setShowModal}
                />
                    :
                    <div
                        className={isModal
                            ? 'service-content-container service-content-container-modal'
                            : 'service-content-container'
                        }>
                        <div className='modal__content'>
                            <p className='service-content-container-title'>{filteredData[0]?.title}</p>
                            <p className='service-content-container-info'>{filteredData[0]?.content}</p>
                        </div>

                    </div>}
            </div>
        </div>
    )
}

export default Services