import { useState, useRef, useEffect } from 'react'
import { SliderData } from './SliderData'
import { IoArrowForward, IoArrowBack } from 'react-icons/io5'
import './slider.css'

const Slider = ({ id }) => {

    const [current, setCurrent] = useState(0);
    const length = SliderData.length;
    const timeout = useRef(null);

    useEffect(() => {
        const nextSlide = () => {
            setCurrent(current => (current === length - 1 ? 0 : current + 1))
        }

        timeout.current = setTimeout(nextSlide, 5000);

        return function () {
            if (timeout.current) {
                clearTimeout(timeout.current)
            }
        }

    }, [current, length]);

    const nextSlide = () => {
        if (timeout.current) {
            clearTimeout(timeout.current)
        }

        setCurrent(current === length - 1 ? 0 : current + 1);
    }

    const prevSlide = () => {
        if (timeout.current) {
            clearTimeout(timeout.current)
        }
        setCurrent(current === 0 ? length - 1 : current - 1)
    }

    if (Array.isArray(SliderData) || SliderData.length <= 0) {
        // return null
    }

    return (
        <div className='slider-section' id={id}>
            <div className='hero-wrapper'>
                {SliderData.map((slide, index) => (
                    <div className='hero-slide' key={index}>
                        {index === current && (
                            <div className='hero-slider'>
                                <img className='hero-image'
                                    src={slide.image}
                                    alt={slide.alt}
                                />
                                <div className='hero-content'>
                                    <h1>{slide.title}</h1>

                                </div>
                            </div>
                        )}

                    </div>
                ))}
                
                <div className='slider-buttons'>
                    <IoArrowBack onClick={prevSlide} className="arrow-buttons" />
                    <IoArrowForward onClick={nextSlide} className="arrow-buttons" />
                </div>
            </div>
        </div>
    )
}

export default Slider