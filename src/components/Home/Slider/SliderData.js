import sliderImg1 from '../../../assets/slider/IMG_1185.jpg'
import sliderImg2 from '../../../assets/slider/IMG_0074 copy.jpg'
import sliderImg3 from '../../../assets/slider/IMG_0849.jpg'
import img2 from '../../../assets/slider/2.jpg'

export const SliderData = [
    {
        title: 'МЦ Тринити предлага висококачествена доболнична помощ в областта на Акушерството и Гинекологията.',
        label: 'View home',
        image: sliderImg1,
        alt: 'image 1'
    },
    {
        title: 'Не техниката е най-важната, но е хубаво когато тя е от най-висок клас!',
        label: 'View home',
        image: img2,
        alt: 'image 2'
    },
    {
        title: 'Не техниката е най-важната, но е хубаво когато тя е от най-висок клас!',
        label: 'View home',
        image: sliderImg3,
        alt: 'image 3'
    },
]