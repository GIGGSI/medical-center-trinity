import AboutUs from "./AboutUs/AboutUs";
import Slider from "./Slider/Slider";
import Team from "./Team/Team";
import Services from "./Services/Services";
import Contacts from "./Contacts/Contacts";
import BlogSection from "./BlogSection/BlogSection";


export {
    AboutUs,
    Slider,
    Team,
    Services,
    Contacts,
    BlogSection
}