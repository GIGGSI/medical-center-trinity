import React from 'react'
import Title from '../../Globals/Title/Title'
import Spinner from '../../Globals/Spinner/Spinner'
import { Link } from 'react-router-dom'

import { blogData } from './Blogs'

import './BlogSection.css'

const BlogSection = ({ id }) => {
    return (
        <div id={id} className="blog-wrapper">
            <Title title="Тринити блог" />
            <p className='blog-info'>Интересни и актуални теми, избрани за теб от <Link to='/blog'><span>Тринити Блог.</span></Link></p>

            <div className='blog-content'>
                {blogData.slice(-3).map((item) => (
                    <div className='blog-content-single' key={item.id}>
                        <Link to={`/single-blog/${item.id}`}>
                            <div className='blog-content-single-image-container'>
                                {
                                    item.imgUrl
                                        ? <img src={item.imgUrl} alt={item.title} />
                                        : <Spinner />
                                }
                            </div>
                        </Link>

                        <div className='blog-content-single-image-container-content'>
                            <Link to={`/single-blog/${item.id}`} style={{display:'inlineBlock'}}>
                                <p className='blog-content-single-title'>{item.title}</p>
                            </Link>
                            <p className='blog-content-single-content'>
                                {item.info.length > 180
                                    ? item.info.slice(0, 180) + '...'
                                    : item.info
                                }
                            </p>
                        </div>

                    </div>
                ))}
            </div>
        </div>
    )
}

export default BlogSection