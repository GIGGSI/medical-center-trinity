import image3 from '../../../assets/blog/img3.jpg'
import planing from '../../../assets/blog/planing.jpeg'
import second from '../../../assets/blog/second.jpg'
import images from '../../../constants/images'


export const blogData = [
    {
        id: 1,
        is_recommended: true,
        title: 'Планиране на вашата бременност',
        info: `Ако мислите за бременност, посетете Вашия лекар за консултация преди зачеването. Той ще ви предостави експертни съвети относно планирането на бременността ви.
        Периодът преди зачеването (3 до 6 месеца преди бременността) е времето за промени в живота, които могат да помогнат за повишаване на плодовитостта, намаляване на проблемите по време на бременност и подпомагане на възстановяването след раждането.`,
        imgUrl: planing,
        secondImg: second,
        content: [
            {
                id: 1,
                heading: 'Фолиева киселина',
                text: '<p>Ако вие и вашият партньор планирате да забременеете, трябва да започнете да приемате фолиева и йодна добавка преди да забременеете. Фолиевата киселина помага за осигуряване на най-добри резултати за здравето на вашето бебе, когато то расте. Ежедневният прием на фолиева киселина преди и по време на бременност също предотвратява появата на дефекти на невралната тръба, при вашето бебе. Йодът е важен за развитието на мозъка на бебето.</p>'
            },
            {
                id: 2,
                heading: 'Внимавайте как се храните',
                text: '<p>Ако вие и вашият партньор се подготвяте за бременност, трябва да погледнете диетата си и да видите къде може да сте в състояние да направите по-здравословен избор на храна. Яденето на добре балансирана диета, включваща много пресни плодове и зеленчуци, ще помогне за шансовете ви за зачеване и здравословна бременност.</p>'
            },
            {
                id: 3,
                heading: 'Алкохол',
                text: '<p>Няма безопасно количество алкохол за пиене по време на бременност; следователно, за жени, които са бременни или планират бременност, непиенето е най-безопасният вариант. Алкохолът може да повлияе на здравето и развитието на нероденото бебе за цял живот.</p>'
            },
            {
                id: 4,
                heading: 'Пушенето',
                text: '<p>Спирането на тютюнопушенето преди бременността е най-ефективното средство за защита на вашето бебе и себе си от развитието на сериозни усложнения по време на бременност. Отказвайки цигарите, е по-вероятно да забременеете естествено и без забавяне, по-малко вероятно да претърпите спонтанен аборт или извънматочна бременност и по-малко вероятно да родите бебето си преждевременно.</p>'
            },
            {
                id: 5,
                heading: 'Преглед преди бременността',
                text: '<p>Добра идея е да поговорите с Вашия лекар, ако планирате да забременеете. Може да има някои изследвания, които да обмислите да направите, както и да обсъдите общото ви здраве и фамилна анамнеза. Това може също да включва обмисляне на ваксинации, здравни прегледи преди бременността, като скрининг на шийката на матката, скрининг на ППИ и зъболекарски прегледи и обсъждане на промени в начина на живот. Има също така възможност да обмислите генетичен скрининг за носители за някои генетични състояния, за които може да сте изложени на риск да предадете на вашето бебе, за които не сте знаели.</p> '
            },
            {
                id: 6,
                heading: 'Най-доброто време за забременяване',
                text: `<p>Месечният цикъл на жената.</p>
                <p> Овулацията настъпва всеки месец, когато една яйцеклетка се отделя от един от яйчниците.</p>
                <p> Понякога се отделя повече от едно яйце, обикновено в рамките на 24 часа след първото яйце. В същото време лигавицата на матката започва да се уплътнява и слузта в шийката на матката става по-тънка, така че сперматозоидите да могат да плуват през нея по-лесно.</p>
                <p> Яйцето започва бавно да се движи надолу по фалопиевата тръба. Ако мъж и жена са правили секс наскоро, яйцеклетката може да бъде оплодена тук от спермата на мъжа.</p>
                <p> Лигавицата на матката вече е достатъчно дебела, за да може яйцеклетката да бъде имплантирана в нея, след като е била оплодена.</p>
                <p> Ако яйцеклетката не е оплодена, тя излиза от тялото по време на месечния цикъл на жената, заедно с лигавицата на утробата, която също се отделя. Яйцето е толкова малко, че не се вижда.</p>`
            },
            {
                id: 7,
                heading: 'Забременяване',
                text: `<p> Най-вероятно е да забременеете, ако правите секс в рамките на един ден след овулацията (освобождаване на яйцеклетка от яйчника). Овулацията настъпва 14 дни преди първия ден от следващата ви менструация (не след това). Средният цикъл отнема 28 дни, но по-кратките или по-дълги цикли са нормални. Така че жените с 28-дневен цикъл ще овулират на 14-ия ден, но жените с 30-дневен цикъл ще овулират на 16-ия ден.</p>
                <p> Яйцето живее около 12 до 24 часа след освобождаването му. За да се случи бременност, яйцеклетката трябва да бъде оплодена от сперма в рамките на това време. Ако искате да забременеете, правенето на секс на всеки няколко дни ще означава, че винаги има сперма, която чака във фалопиевите тръби, за да срещне яйцеклетката, когато бъде освободена.</p>
                <p>  Сперматозоидите могат да живеят около 5 дни в тялото на жената. Така че, ако сте правили секс в дните преди овулацията, спермата ще има време да пътува нагоре по фалопиевите тръби, за да „изчака“ яйцеклетката да бъде освободена. Трудно е да се знае точно кога настъпва овулацията, освен ако не практикувате естествено семейно планиране или осъзнаване на плодовитостта.</p>`
            }
        ]
    },
    {
        id: 2,
        is_recommended: true,
        title: 'Подгответе тялото си за бременността',
        info: `Когато сте бременна, тялото ви има важна работа за 9 месеца. Така че, ако планирате скоро да имате бебе, направете няколко прости стъпки сега, за да се подготвите за здраво бебе.`,
        imgUrl: images.trainingWoman,
        secondImg: images.womanDrinkingPillows,
        content: [
            {
                id: 1,
                heading: 'Стигнете до здравословно тегло',
                text: `<p> Ако сте с наднормено или поднормено тегло, може да е по-трудно да забременеете. Теглото ви влияе дали яйчниците ви ще отделят яйцеклетка или ще овулират всеки месец. Излишните килограми също ви правят по-вероятно да имате определени здравословни проблеми по време на бременност, като гестационен диабет или прееклампсия, вид високо кръвно налягане.</p> 
                <p>  Бихте могли да забременеете по-лесно, ако отслабнете или наддадете на тегло, преди да започнете да се опитвате да забременеете. Яжте здравословни храни и спортувайте редовно. Ако сте с много наднормено тегло, не се притеснявайте да се опитвате да отслабнете до половината от размера си. Отслабването дори с няколко килограма ще помогне.</p> 
                `
            },
            {
                id: 2,
                heading: 'Приемайте витамини',
                text: `<p> Важно е да започнете да приемате пренатални витамини преди да забременеете. Защо? Защото вероятно няма да разберете, че сте бременна, докато не пропуснете менструация. Това е седмици след като бебето ви започне да расте. Ако чакате толкова дълго, за да приемате витамини, може да пропуснете важна защита.</p> 
                <p>  Приемайте  фолиева киселина всеки ден. Тя помага за предотвратяване на вродени дефекти в мозъка и гръбначния стълб на вашето бебе. Пренаталните витамини също съдържат желязо, което е полезно и за двама ви. Той помага на мускулите на вашето бебе да растат и ви помага да избегнете анемия, когато тялото ви има твърде малко червени кръвни клетки. Калцият е от ключово значение за костите, мускулите, нервите и сърцето на майката и детето.</p> 
                `
            },
            {
                id: 3,
                heading: 'Помислете за химикалитe',
                text: '<p> Някои от тях, като пестициди, разтворители и торове, могат да ви затруднят да забременеете или да навредят на бебето ви след забременяване. Помислете кои може да са около вас у дома и на работа. Говорете с Вашия лекар какво е безопасно и какво трябва да избягвате. Ако работата ви включва да сте близо до нещо рисково, като радиация, живак или олово, попитайте работодателя си как можете да се защитите или вижте дали можете да промените задълженията си. </p> '
            },
            {
                id: 4,
                heading: 'Направете консултация с Вашият лекар',
                text: `<p> Вашият акушер/гинеколог ще се увери, че сте в добра форма и ще говори с вас за всички здравословни състояния, които имате и какви лекарства приемате. Ще обсъдите и вашата диета, упражнения и други навици.</p> 
                <p>  Ако имате здравословно състояние като високо кръвно налягане, депресия, диабет или гърчове, посетете Вашия лекар и го уведомете, че планирате да забременеете. Може да ви кажат да контролирате здравето си, преди да се опитате да имате бебе. Ако приемате лекарства, може да ви предложат да преминете към лекарство, което е безопасно за бъдещите майки. Но не спирайте да приемате лекарствата си, без лекарят да е наред.</p> `
            },
            {
                id: 5,
                heading: 'Откажете се от лошите навици',
                text: '<p> Тютюн, алкохол, марихуана и други наркотици могат да причинят вродени дефекти и други сериозни здравословни проблеми, така че спрете да ги използвате, преди да се опитате да забременеете. Може да рискувате да навредите на бебето си, преди дори да разберете, че сте бременна.</p> '
            }
        ]
    },
    {
        id: 3,
        is_recommended: false,
        title: 'Фолиева киселина и бременност',
        info: `
        Фолатът и фолиевата киселина са важни за бременността, тъй като могат да помогнат за предотвратяване на вродени дефекти, известни като дефекти на невралната тръба.
        `,
        imgUrl: images.follatFoods,
        secondImg: images.follat,
        content: [
            {
                id: 1,
                heading: '',
                text: `<p> Фолатът е витамин от група В, необходим за здравословен растеж и развитие. Известен е като „фолат“, когато се среща естествено в храната, като например зеленолистни зеленчуци, и като „фолиева киселина“, когато се добавя към храна, като например хляб и зърнени закуски, или се използва в хранителни добавки.</p>
               <p> Spina bifida е един от най -честите вродени дефекти. Това се случва през първите седмици от бременността, когато се образуват мозъкът и гръбначният мозък.</p>
               <p> Повечето случаи на дефекти на невралната тръба могат да бъдат предотвратени, ако имате достатъчно фолат преди и по време на ранна бременност.</p>
               <p> Можете да си набавите достатъчно фолат, като ядете храни, богати на фолиева киселина и приемате добавка.</p>`
            },
            {
                id: 2,
                heading: 'Добавки с фолиева киселина',
                text: `<p>Като цяло, когато се опитвате да забременеете или в първите месеци на бременността, ще трябва да потърсите добавки, които съдържат фолиева киселина. Това обикновено са добавки, които съдържат само фолиева киселина или специални добавки за бременност.</p> <p> Мултивитаминните добавки обикновено не съдържат достатъчно фолиева киселина.
                Най -добрият начин да гарантирате, че ще получите достатъчно фолиева киселина е да приемате дневна добавка с фолиева киселина поне 1 месец преди и до 3 месеца след зачеването. Не е необходимо да приемате добавки с фолиева киселина след това.</p>`
            },
            {
                id: 3,
                heading: 'Храни, богати на фолати',
                text: `<p>Много храни са естествено богати на фолат, но фолатът се разтваря във вода и лесно се унищожава при готвене. Най - добре е зеленчуците да се варят леко или да се ядат сурови. Готвенето на пара се препоръчва. </p>
                <p> Следните храни са добри източници на естествени фолати:</p>
                  <ul class="blog-ul">
               <li> зеленчуци (броколи, брюкселско зеле, зеле, карфиол, английски спанак, зелен фасул, маруля, гъби, пащърнак, сладка царевица, тиквички)</li>
               <li> плодове (авокадо, грейпфрут, портокал)</li>
               <li> бобови растения (нахут, соеви зърна, боб лима, червен боб, леща, боб харикот) </li>
               <li> яйца</li>
               <li> ядки</li>
               <li> сокове (много ябълкови и портокалови сокове</li>
                  </ul>  
                    `
            },
            {
                id: 4,
                heading: 'По-високи дози фолиева киселина',
                text: `<p>Някои жени имат повишен риск от бременност, засегната от дефект на невралната тръба и се препоръчва да приемат по -висока доза (5 mg) фолиева киселина всеки ден, докато не настъпи 12 -та седмица от бременността. Жените имат повишен риск, ако:</p>
                <ul class="blog-ul">
                 <li> те или техният партньор имат дефект на невралната тръба </li>      
                 <li> те или техният партньор имат фамилна анамнеза за дефекти на невралната тръба </li>        
                 <li>  те са имали предишна бременност, засегната от дефект на невралната тръба </li>    
                 <li>  имат диабет </li>
                 <li>те имат индекс на телесна маса (BMI) по -голям от 30 </li>
                 <li> те имат риск да не абсорбират добре хранителните вещества </li>
                </ul>
               <p> В допълнение, жените, които приемат антиепилептични лекарства, трябва да се консултират с лекаря си за съвет, тъй като може да се наложи да приемат и по-висока доза фолиева киселина.</p>`
            }
        ]
    },
    {
        id: 4,
        is_recommended: true,
        title: 'Най-честите въпроси свързани с кърменето. ',
        info: 'Прекрасно е, че планирате да кърмите вашето бебе! Тъй като ползите от майчиното мляко са неизброими. През първите шест месеца от живота си, е препоръчително, бебето да получава само майчина кърма, без допълнителни храни и течности (сок, чай и дори вода!).',
        imgUrl: images.womanBreastfeeding,
        secondImg: images.womanBreastfeeding2,
        content: [
            {
                id: 1,
                heading: '',
                text: '<p> След шестия месец кърменето се допълва с подходящи за възрастта храни и започва предлагане на вода. Добре е да кърмите до втората година, а ако вие и детето желаете, може да продължите и по-дълго. </p> '
            },
            {
                id: 2,
                heading: 'Как да кърмя бебето?',
                text: '<p>Кърмените деца се развиват най-добре, когато се слагат на гърда толкова често и толкова дълго, колкото искат. През първия месец след раждането бебето трябва да суче не по-малко от 8 пъти за 24 часа – може да се наложи да го будите за хранене, ако редовно проспива големи интервали.</p> '
            },
            {
                id: 3,
                heading: 'Защо е важно кърменето?',
                text: ` <ul class="blog-ul">
                 <li> <strong>Когато се храни с вашето мляко</strong;></li> 
                 <li> бебето расте и се развива нормално;</li>
                 <li>  получава защита срещу различни инфекции и се стимулира имунната му система;</li>
                 <li>   по-рядко страда от заболявания като диария и възпаления на ушичките;</li>
                 <li>  намалява се рискът от алергии, астма и атопичен дерматит;</li>
                 <li> честият контакт, общуването и майчиното мляко подпомагат интелектуалното му развитие.</li>
                 </ul>
               `

            },
            {
                id: 4,
                heading: 'Кърменето е полезно и за вас самата:',
                text: `<ul class="blog-ul">
                 <li>Чрез контакта и общуването по време на кърмене лесно се създава силна емоционална връзка между вас и детето.</li>
                 <li> Кърменето помага по-бързо да възстановите теглото си след раждането.</li>
                 <li> Забавя се възстановяването на менструацията.</li>
                 <li> Костите ви стават по-здрави.</li>
                 <li>  Кърменето намалява риска от рак на гърдата и яйчниците.</li>
                 </ul>`
            },
            {
                id: 5,
                heading: 'За семейството кърменето означава, че:',
                text: `
                <ul class="blog-ul">
                 <li> имате здраво, спокойно и добре развиващо се дете; </li> 
                 <li>  пътувате лесно и удобно, без притеснение за храненето на бебето;</li> 
                 <li>  посещавате лекаря основно за консултация </li> 
                </ul>
           `
            }
        ]
    },
    {
        id: 5,
        is_recommended: false,
        title: 'Подобряване на мъжкото здраве преди зачеването',
        info: 'Здравето и възрастта на бъдещия баща могат да повлияят на шансовете на партньора му да забременее, както и на бъдещото здраве на бебето. Има прости стъпки, които можете да предприемете, за да гарантирате, че сте възможно най-здрави и годни, за да станете татко.',
        imgUrl: images.man,
        secondImg: images.man2,
        content: [
            {
                id: 1,
                heading: '',
                text: `
                 <p>Ако искате да заченете бебе, важно е да: </p>
                 <ul class="blog-ul">
                 <li> започнете с опитите преди да сте навършили 40, ако е възможно</li>
                 <li> бъдете в здравословен диапазон на теглото</li>
                 <li> откажете цигарите</li>
                 <li> намалите алкохола</li>
                 <li> избягвайте излагането на някои химикали</li>
                 </ul>
                `
            },
            {
                id:2,
                heading:'За твоята възраст ',
                text:`
                 <p>Мъжете могат да произвеждат сперма до 70-те и повече години, но качеството на спермата намалява с напредване на възрастта. Мъжете над 40 години имат по-малко здрави сперматозоиди от по-младите мъже. Ако сте на 45 или повече години, на партньора ви може да отнеме повече време, за да забременее и е изложен на по-голям риск от спонтанен аборт. По-възрастните татковци са изложени на малко по-висок риск да имат бебе с аутизъм или психично заболяване като шизофрения, в сравнение с по-младите бащи.</p>
                 <p>Разбира се, не можете да контролирате възрастта си – но ако сте по-възрастни и мислите за зачеване, започнете да опитвате възможно най-скоро.</p>
                `
            },
            {
                id:2,
                heading:'Вашето здраве има значение',
                text:`
                  <p>Около половината от случаите на безплодие се дължат на проблем, който мъжът има, така че е добра идея да се подложите на общ медицински преглед, преди да опитате за бебе. Говорете с Вашия лекар за всички изследвания, от които може да се нуждаете, и дали трябва да избягвате лекарства, които може би вече приемате.</p>
                 <p>Имате по-голям шанс да забременеете, ако имате здрава сперма. Най-добрият начин да гарантирате, че имате много здрава сперма, е да поддържате тестисите си хладни, тъй като топлината влияе върху способността на тестисите да произвеждат сперма. В продължение на няколко месеца, преди да искате да забременеете, можете да поддържате тестисите си хладни, като избягвате много горещи бани или спа, не поставяте лаптопа си в скута си и носите свободно бельо.</p>
                 <p>Ако планирате бебе, проверете дали имате някакви полово предавани инфекции (ППИ), тъй като те могат да доведат до безплодие. Провеждането на тест преди зачеването намалява риска от предаване на инфекция на вашия партньор или детето.</p>
                `
            },
            {
                id:3,
                heading:'Допълнителни или алтернативни лекарства',
                text:`
                 <p>Допълнителните или алтернативни терапии, като акупунктура, билкови лекарства и масаж, могат да подобрят общото ви благосъстояние, но няма доказателства, че могат да повишат плодовитостта ви. Винаги е най-добре да говорите с Вашия лекар, преди да започнете каквото и да е допълнително или алтернативно лечение.</p>
                `
            },
            {
                id:4,
                heading:'Вашият начин на живот влияе на плодовитостта ви',
                text:`
                 <p>Здравословното тегло увеличава шансовете ви за зачеване на здраво бебе. Наднорменото тегло влияе върху качеството на спермата ви, намалява сексуалното ви желание и може да затрудни поддържането на ерекция. Поднорменото тегло също може да ви затрудни да забременеете.</p>
                 <p>Най-добрият начин за постигане на здравословно тегло е да ядете питателна диета и да тренирате редовно. Ако сте с наднормено тегло или затлъстяване, дори загубата на само няколко килограма ще ви помогне. Вие и вашият партньор трябва да се насърчавате взаимно да водите здравословен начин на живот. </p>
                 <p>Всяка седмица се опитвайте да правите 2½ до 5 часа физическа активност с умерена интензивност или 1¼ до 2½ часа физическа активност с енергичен интензитет. Просто да седите по-малко и да се движите повече ще помогне.</p>
                `
            },
            {
                id:5,
                heading:'Неща, които трябва да избягвате',
                text:`
                 <p><strong>Пушене:</strong> Няма безопасна граница за пушене. Ако се опитвате да имате бебе, важно е да се откажете поне 3 месеца преди да започнете да опитвате за бебе. Пушенето може да повлияе на ерекцията ви, уврежда ДНК в спермата ви и пушенето на повече от 1 кутия цигари на ден увеличава риска бебето да развие левкемия.</p>
                 <p><strong>Пиене: </strong> Тежкото пиене засяга сексуалното ви желание, затруднява поддържането на ерекция и влияе върху качеството на спермата ви. Не е нужно да се отказвате от алкохола – просто намалете и поддържайте приема си в безопасни граници. Няма доказателства, че консумацията на кофеин (например кафе) ще повлияе на шанса на мъжа да зачене.</p>
                 <p><strong>Лекарства:</strong>  Вашият лекар ще ви уведоми дали е добре да продължите да приемате лекарствата си и някакви витамини или добавки. Лекарствата, които могат да повлияят на вашата плодовитост, включват болкоуспокояващи, които съдържат опиати, както и лекарства за лечение на депресия и тревожност. Лечението на рак като химиотерапия и лъчетерапия също може да повлияе на вашата плодовитост.</p>
                 <p><strong>Стероиди: </strong> Приемането на анаболни стероиди може да ви спре да произвеждате сперма. Те могат да свият тестисите ви, като същевременно повлияят на ерекцията и сексуалното ви желание. Може да отнеме до 2 години след спиране на анаболните стероиди, преди спермата на човек да бъде отново здрава.</p>
                 <p><strong>Наркотици:</strong>  Избягвайте всички наркотици за развлечение като кокаин, хероин, екстази и марихуана, тъй като те могат да причинят трайни проблеми с плодовитостта.</p>
                 <p><strong>Химикали: </strong> Токсините и замърсителите както по време на работа, така и в ежедневието могат да повлияят на спермата ви. Те включват пестициди, тежки метали, някои химикали и пластмаси и радиация. Опитайте се да ограничите излагането си на тях и носете защитно облекло, ако се опитвате да забременеете.</p>
                `
            }
        ]

    }
]