import { Link } from 'react-router-dom'

import { ourTeam } from './TeamConstants'
import { Title } from '../../Globals'

import './Team.css'

const Team = ({ id }) => {
    return (
        <div id={id} className="team-wrapper">
            <Title title='Нашият Екип' />

            <div className='team-content'>
                {ourTeam.map((item) => (
                    <div
                        key={item.id}
                        className="team-single-doctor"
                    >
                        <Link
                            to={`/${item.urlName}`}
                            className='team-single-doctor-image-container'
                        >
                            <img src={item.imgPath} alt={item.name} />
                        </Link>
                        <Link
                            to={`/${item.urlName}`} className='team-content-name' >
                            {item.name}
                        </Link>


                    </div>
                ))}
            </div>

        </div>
    )
}

export default Team