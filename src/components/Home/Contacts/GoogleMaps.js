import React from 'react'

import { useJsApiLoader, GoogleMap, Marker } from '@react-google-maps/api'

import Spinner from '../../Globals/Spinner/Spinner'

const GoogleMaps = () => {
    const { isLoaded } = useJsApiLoader({
        googleMapsApiKey: 'AIzaSyApYPyndC-MxmzjfVUyUkrO58OYss4u2js'
    })

    let center = {
        lat: 42.693404,
        lng: 23.3500537
    }

    if (isLoaded) {
        return <GoogleMap
            center={center}
            zoom={15}
            mapContainerStyle={{
                width: '100%',
                height: '100%'
            }}
            options={{
                zoomControl: false,
                streetViewControl: false,
                mapTypeControl: false
            }}
        >
            <Marker position={center} />
        </GoogleMap>
    } else {
        return <Spinner />
    }

}

export default GoogleMaps