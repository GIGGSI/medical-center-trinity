import React from 'react'
import GoogleMaps from './GoogleMaps'

import { Title } from '../../Globals'

import { BiMap, BiPhone } from 'react-icons/bi'
import { AiOutlineClockCircle } from 'react-icons/ai'
import './Contacts.css'

const Contacts = ({ id }) => {
    return (
        <div id={id} className='contacts-wrapper'>
            <Title title="Контакти" />

            <div className='contacts-wrapper-container'>
                <div className='contacts-content'>
                    <a className='contacts-content-a'
                        href='https://www.google.com/maps/place/%D0%9C%D0%B5%D0%B4%D0%B8%D1%86%D0%B8%D0%BD%D1%81%D0%BA%D0%B8+%D0%A6%D0%B5%D0%BD%D1%82%D1%8A%D1%80+%D0%A2%D1%80%D0%B8%D0%BD%D0%B8%D1%82%D0%B8/@42.693404,23.3500537,17.15z/data=!4m5!3m4!1s0x0:0x9168b2b1c11afe91!8m2!3d42.6942389!4d23.349334'
                        target='_blank'
                        rel="noreferrer"
                    >
                        <p><span className='contacts-content-icon'><BiMap />Aleko Konstantinov 17 A 1505 Sofia.</span></p>
                    </a>
                    <a
                        href='tel:0895690009'
                    >
                        <p><span className='contacts-content-icon'><BiPhone />089 569 0009</span></p>

                    </a>
                    <p><span className='contacts-content-icon'><AiOutlineClockCircle /> Работно Време: Пон - Пет: 09 - 18:00</span></p>
                    <p>Как да ни намерите?</p>
                    <p className='contacts-content-small'>
                        Медицински център „Тринити“ се намира на удобна локация – на пешеходно разстояние от търговски център Сердика - (кв. Оборище).
                    </p>
                </div>
                <div className='contacts-map-container'>
                    <GoogleMaps />
                </div>
            </div>


        </div>
    )
}

export default Contacts