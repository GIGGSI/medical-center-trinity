import { useState } from 'react'
import Title from '../../Globals/Title/Title'
import images from '../../../constants/images'
import { HiPlus, HiMinus } from 'react-icons/hi'

import './PatientInformation.css'
const data = [
    {
        id: 1,
        question: 'Как да си запиша час?',
        answer: `Ако желаете да си уговорите час с някой от нашите практикуващи, моля, свържете се с нашия персонал на рецепцията. Като алтернатива можете да резервирате своите срещи онлайн. Ще бъдат положени всички усилия, за да се съобразим с предпочитаното от вас време и избор на практикуващ.`
    },
    {
        id: 2,
        question: 'как да си запиша час?',
        answer: `Ако желаете да си уговорите час с някой от нашите практикуващи, моля, свържете се с нашия персонал на рецепцията. Като алтернатива можете да резервирате своите срещи онлайн. Ще бъдат положени всички усилия, за да се съобразим с предпочитаното от вас време и избор на практикуващ.`
    },
    {
        id: 3,
        question: 'как да си запиша час?',
        answer: `Ако желаете да си уговорите час с някой от нашите практикуващи, моля, свържете се с нашия персонал на рецепцията. Като алтернатива можете да резервирате своите срещи онлайн. Ще бъдат положени всички усилия, за да се съобразим с предпочитаното от вас време и избор на практикуващ.`
    },
    {
        id: 4,
        question: 'как да си запиша час?',
        answer: `Ако желаете да си уговорите час с някой от нашите практикуващи, моля, свържете се с нашия персонал на рецепцията. Като алтернатива можете да резервирате своите срещи онлайн. Ще бъдат положени всички усилия, за да се съобразим с предпочитаното от вас време и избор на практикуващ.`
    },
    {
        id: 5,
        question: 'как да си запиша час?',
        answer: `Ако желаете да си уговорите час с някой от нашите практикуващи, моля, свържете се с нашия персонал на рецепцията. Като алтернатива можете да резервирате своите срещи онлайн. Ще бъдат положени всички усилия, за да се съобразим с предпочитаното от вас време и избор на практикуващ.`
    }
]
const PatientInformation = () => {
    const [isOpenContent, setIsOpenContent] = useState(false)

    const toggle = index => {
        if (isOpenContent === index) {
            return setIsOpenContent(null);
        }
        setIsOpenContent(index);
    };

    return (
        <div className='patient-information-wrapper'>

            <div className='patien-information-content'>
                <div className='pation-information-content-wrapper'>

                    <Title title="Информация за пациентите" />

                    {data.map((item) => (
                        <div key={item.id} className="partion-information-wrapper">
                            <p
                                className='pation-information-title'
                                onClick={() => toggle(item.id)}
                            >
                                <span> {item.id}.</span>{item.question}
                                {isOpenContent === item.id
                                    ? <HiMinus className='pation-information-icon' />
                                    : <HiPlus className='pation-information-icon' />}
                            </p>

                            {
                                isOpenContent === item.id
                                && <p className='pation-information-content'>
                                    {item.answer}
                                </p>
                            }
                        </div>
                    ))}
                </div>

                <div className={isOpenContent ?
                    `patien-information-image-container 
                patien-information-image-container-big`
                    : 'patien-information-image-container'}>
                    <img src={images.doc} alt="patient information" />
                </div>

            </div>
        </div>
    )
}

export default PatientInformation